# tmchk

Another time server (and client).


## Installation

`tmchk` comes with a handy script to install itself in your system, the only thing you need to do is clone the repo and run the script:

```bash
$ git clone git://github.com/nathanpc/tmchk.git
$ cd tmchk
$ sudo ./install.sh
```


## Usage

It's very simple to use `tmchk`. First you need to start the server which can be done by simply executing:

    $ tmchk

To get the time from the server you just need to execute the script like this:

    $ tmchk <server> <strftime format string>
    $ tmchk 127.0.0.1 "Now it's %I:%M%p."

You can learn more about [strftime strings here](http://www.cplusplus.com/reference/ctime/strftime/).


## Why Perl?

I choose [Perl](http://perl.org/) because this project was intended to sync the time between my home development server and my old, but awesome, [HP Jornada 720 running Linux](http://pic.twitter.com/e8a38S8n), which sadly can't store the correct time because of how Linux runs on it. Perl is the only interpreted language that fully works on it, Ruby (no RubyGems) and Python (no pip/easy_install) are broken.
