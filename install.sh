#!/bin/sh

echo "Installing tmchk"
echo ""

echo "install ./tmchk.pl /usr/bin/"
install ./tmchk.pl /usr/bin/

echo "mv /usr/bin/tmchk.pl /usr/bin/tmchk"
mv /usr/bin/tmchk.pl /usr/bin/tmchk

echo "chmod a+x /usr/bin/tmchk"
chmod a+x /usr/bin/tmchk

echo ""
echo "tmchk installed!"