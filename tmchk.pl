#!/usr/bin/perl
# tmchk.pl
# tmchk Server (and client)

use warnings;
use IO::Socket::INET;
use POSIX;


my ($socket, $client);
my ($peer_address, $peer_port);

sub handle_data {
	($command, $arg) = split(/\s/, $_[0], 2);
	
	if ($command eq "TIME") {
		$time = POSIX::strftime($arg, localtime());
		$client->send("$time\r\n");
	} elsif ($command eq "QUIT") {
		$client->close();
	}
}

sub socket_client {
	($server, $tmformat) = @_;

	# Setup the socket.
	$socket = new IO::Socket::INET(
		PeerHost => $server,
		PeerPort => "2013",
		Proto    => "tcp"
	) or die "Couldn't connect to $server\n$@\n$!";

	# Send TIME request.
	$socket->send("TIME $tmformat\r\n");

	# Receive the formatted string from server.
	$data = substr(<$socket>, 0, -2);
	print "$data\n";

	# Send QUIT message.
	$socket->send("QUIT\r\n");
	$socket->close();
}

sub server {
	# Setup socket.
	$socket = new IO::Socket::INET(
		LocalPort => "2013",
		Proto     => "tcp",
		Listen    => 5,
		ReuseAddr => 1
	) or die "Error while trying to create socket: $!\n";

	print "tmchk waiting for connections on port 2013\n";
	while (1) {
		# Waiting for new connections.
		$client = $socket->accept();

		# Get host and port of the client.
		$peer_address = $client->peerhost();
		$peer_port = $client->peerport();

		print "\nNew connection from $peer_address:$peer_port\n";

		# Get the client message and remove the \r\n at the end.
		$data = substr(<$client>, 0, -2);
		print "$peer_address:$peer_port requested \"$data\"\n";

		handle_data($data);
	}
	
	$socket->close();
}

sub usage {
	print "Usage: tmchk [server_location] [time string]\n";
	print "\nYou can learn more about strftime strings here at http://www.cplusplus.com/reference/ctime/strftime/\n";
}

if (@ARGV eq 0) {
	server();
} elsif (@ARGV eq 2) {
	socket_client(@ARGV);
} else {
	usage();
}